from mapswell import Renderer

if __name__ == '__main__':
    # La Reunion
    # Top-Left = -20.846367, 55.183157
    # Bottom-Right = -21.417024, 55.861562

    r = Renderer('/home/remy/dev/mapswell/assets/rb.xml')

    with r.render(1201, 660, 1.5, (55.183157, -21.417024, 55.861562, -20.846367)) as m:
        f = open('/tmp/map.png', 'w')
        f.write(m.read())
        f.close()
