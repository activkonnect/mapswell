# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# Mapswell
# (c) 2014 ActivKonnect
# Rémy Sanchez <remy.sanchez@activkonnect.com>
# Under the terms of the WTFPL


def smart_bytes(s):
    if isinstance(s, unicode):
        return s.encode()
    else:
        return str(s)
