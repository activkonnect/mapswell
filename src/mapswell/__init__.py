# vim: fileencoding=utf-8 tw=100 expandtab ts=4 sw=4 :
#
# Mapswell
# (c) 2014 ActivKonnect
# Rémy Sanchez <remy.sanchez@activkonnect.com>
# Under the terms of the WTFPL

from __future__ import unicode_literals

import mapnik
import tempfile
import shutil
import tarfile
from os import path
from ._utils import smart_bytes
from distutils.dir_util import copy_tree

try:
    from xml.etree import cElementTree as et
except ImportError:
    from xml.etree import ElementTree as et


class RenderedMap(object):
    """
    Utility class in order to force the use of a `with` statement on the output of Renderer.
    """

    def __init__(self, m, scale, style, files, src_wd):
        self.m, self.scale, self.style, self.files, self.src_wd = m, scale, style, files, src_wd

    def save_style(self):
        with open(path.join(self.dir, 'style.xml'), 'w') as f:
            f.write(self.style)

    def extract_files(self):
        for name, content in self.files.items():
            with open(path.join(self.dir, name), 'w') as f:
                f.write(content)

    def copy_style_wd(self):
        copy_tree(self.src_wd, self.dir)

    def __enter__(self):
        self.dir = tempfile.mkdtemp()
        self.path = path.join(self.dir, 'map.png')

        self.copy_style_wd()
        self.extract_files()
        self.save_style()

        mapnik.load_map(self.m, smart_bytes(path.join(self.dir, 'style.xml')))

        im = mapnik.Image(self.m.width, self.m.height)
        mapnik.render(self.m, im, self.scale)

        im.save(smart_bytes(self.path))
        self.f = open(self.path, 'rb')

        return self.f

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.f is not None:
            self.f.close()

        if hasattr(self, 'dir'):
            shutil.rmtree(self.dir)


class Renderer(object):
    """
    Can render OSM maps using Mapnik. It is targeted at printable maps rather than the usual
    tiles system.
    """

    def __init__(self, style):
        self.style = smart_bytes(style)
        self.extra_styles = []
        self.extra_files = {}

    def extract_extra(self, extra):
        for f in extra:
            with tarfile.open(None, 'r:gz', f) as t:
                for member in t.getmembers():
                    if member.name == 'pack.xml':
                        self.extra_styles.append(t.extractfile(member).read())
                    elif member.name.startswith('assets/'):
                        self.extra_files[member.name[7:]] = t.extractfile(member).read()

    def make_style(self):
        tree = et.parse(self.style)
        m = tree.getroot()

        for style in self.extra_styles:
            sub_tree = et.fromstring(style)

            for x in sub_tree.iterfind('./*'):
                m.append(x)

        return et.tostring(m)

    def render(self, width, height, scale, extent, extra):
        """
        Renders the map into a file-like object. It will create a PNG image file of the specified
        width and height. The `scale` factor can be used to make things bigger on the map. For
        printing, it is reasonable to use a factor of 2 or 3.

        :param width: int
        :param height: int
        :param scale: float
        :param extent: tuple
        :return: RenderedMap
        """

        extent_box = mapnik.Box2d(*extent)
        m = mapnik.Map(width, height)
        m.zoom_to_box(extent_box)

        self.extract_extra(extra)
        style = self.make_style()

        return RenderedMap(m, scale, style, self.extra_files, path.dirname(self.style))
