argparse==1.2.1
backports.ssl-match-hostname==3.4.0.2
certifi==14.05.14
futures==2.1.6
mapnik2==2.2.0
tornado==4.0.1
wsgiref==0.1.2
requests==2.3.0
